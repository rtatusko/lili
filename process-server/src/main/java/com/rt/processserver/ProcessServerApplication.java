package com.rt.processserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ProcessServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcessServerApplication.class, args);
    }

}
